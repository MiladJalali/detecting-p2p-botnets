import json

from matplotlib import pyplot as plt
from scapy.layers.inet import UDP, TCP
from scapy.sendrecv import sniff
from scipy.cluster.hierarchy import dendrogram, linkage

from flow import Flow, FlowDependency
from params import MAX_FLOW_SIZE, MIN_FLOW_REPEAT_COUNT, FLOW_PACKET_THRESHOLD, MAX_FLOW_DURATION, T_DEP, N_DEP

packets_data = list()


def read_packet_data(packet):
    if packet.haslayer(UDP) or packet.haslayer(TCP):
        packet_size = len(packet)
        source_ip = packet.src
        destination_ip = packet.dst
        send_time = packet.time
        packets_data.append({'packet_size': packet_size,
                             'source_ip': source_ip,
                             'destination_ip': destination_ip,
                             'send_time': send_time,
                             'source_port': packet.sport,
                             'destination_port': packet.dport})


def flow_generation(cleaned_data):
    flows = list()

    # Initialize first flow with first packet
    first_flow = Flow.create_new_flow(cleaned_data[0])
    flows.append(first_flow)

    cleaned_data = cleaned_data[1:]
    for packet in cleaned_data:
        need_new_flow = True
        for flow in flows:
            """
            اگر فاصله زمانی پکت با جدیدترین فلو بیش از مقدار آستانه باشه فلو های قدیمی هم همینطور هستن و نیازی به بررسی ندارن
            """
            if packet['send_time'] - flow.end_time > FLOW_PACKET_THRESHOLD:
                break

            if flow.belong_to_this_flow(packet):
                flow.update(packet['packet_size'], packet['send_time'])
                need_new_flow = False
                break

        if need_new_flow:
            new_flow = Flow.create_new_flow(packet)
            flows.insert(0, new_flow)
    return flows


def filter_flows(flows):
    # delete large flows
    for flow in flows:
        if flow.size > MAX_FLOW_SIZE:
            flows.remove(flow)

    # delete long time flows (Like SSH)
    for flow in flows:
        if flow.end_time - flow.start_time > MAX_FLOW_DURATION:
            flows.remove(flow)

    # calculate repeated count of all flows
    for flow1 in flows:
        for flow2 in flows:
            if flow1.is_equal(flow2):
                flow1.repeated_count += 1

    # delete low repeated count flows
    for flow in flows:
        if flow.repeated_count < MIN_FLOW_REPEAT_COUNT:
            flows.remove(flow)

    return flows


def find_f_h(f_i, index, g_h, T_DEP):
    f_h = list()
    for f_n in g_h[index + 1:]:
        if f_n.start_time - f_i.end_time < T_DEP:
            f_h.append(f_n)
    return f_h


def extract_hosts(flows):
    # find all hosts : equal to Line 2 in algorithm
    hosts = list()
    for flow in flows:
        hosts.append(flow.host1_ip)
        # hosts.append(flow.host2_ip)
    return list(set(hosts))


def two_level_dependency_extract(flows):
    hosts = extract_hosts(flows)

    D = list()
    for host in hosts:
        # find g_h and sort : equal to line 3 ,4 in algorithm
        g_h = list()
        for flow in flows:
            if flow.host1_ip == host:
                g_h.append(flow)
                g_h.sort(key=lambda flow: flow.start_time)

        for index, f_i in enumerate(g_h):  # line 5
            # find flows f_h start after f_i within T_dep
            f_h = find_f_h(f_i, index, g_h, T_DEP)
            for f_j in f_h:
                if abs(f_i.repeated_count - f_j.repeated_count) < N_DEP:  # line 8
                    # creating FlowDependency obj
                    fi_fj = FlowDependency(first_flow=f_i, last_flow=f_j)
                    is_exist = False
                    for flow_dependency in D:
                        if fi_fj.is_equal(flow_dependency):
                            flow_dependency.t_list[0] += 1  # line 10
                            is_exist = True
                            break
                    if not is_exist:
                        D.append(fi_fj)  # line 12

    true_dependencies = list()
    for flow_dependency in D:  # line 13
        flow_dependency.check_dependency()
        if flow_dependency.is_true_dependency:
            true_dependencies.append(flow_dependency)
    return true_dependencies


def multi_level_dependency_extract(true_dependencies):
    for dependency1 in true_dependencies:
        for dependency2 in true_dependencies:
            if dependency1.last_flow.is_equal(dependency2.first_flow):
                dependency1.flows.append(dependency2.first_flow)
                dependency1.t_list.append(dependency2.t_list[0])
                dependency1.last_flow = dependency2.first_flow
    for dependency in true_dependencies:
        dependency.calculate_multi_level_scores()
    return true_dependencies


def calculate_multi_level_score(true_dependencies):
    pass


def calculate_k_level_weight_sum(true_dependencies, host):
    sum = 0
    for d in true_dependencies:
        if (host == d.first_flow.host1_ip):
            sum += d.get_dependency_level
    return sum


def compute_distance(true_dependencies):
    hosts = list()
    for dependency in true_dependencies:
        hosts.append(dependency.first_flow.host1_ip)
    unique_hosts = set(hosts)
    list_host = list(unique_hosts)

    host_i, host_j = (len(list_host), len(list_host))
    distance_matrix = [[0] * host_i] * host_j
    sim = 0
    for indexi, i in enumerate(list_host):
        host_i_all_dependecy = [x for x in true_dependencies if x.first_flow.host1_ip == i]
        for indexj, j in enumerate(list_host):
            host_j_all_dependecy = [x for x in true_dependencies if x.first_flow.host1_ip == j]
            sum = calculate_k_level_weight_sum(true_dependencies, i)
            dist = 0
            for dep_i in host_i_all_dependecy:
                for dep_j in host_j_all_dependecy:
                    if (dep_i.get_dependency_level == dep_j.get_dependency_level):
                        set_i = set(dep_i.flows)
                        set_j = set(dep_j.flows)
                        if (set_i.intersection(set_j) and set_i.union(set_j)):
                            sim = abs(len(set_i.intersection(set_j)) / len(set_i.union(set_j)))
                        dist += (1 - sim) * (dep_i.get_dependency_level / sum)
            distance_matrix[indexi][indexj] = dist
    return distance_matrix


def cluster_hosts(matrix):
    linked = linkage(matrix, 'single')
    plt.figure(figsize=(10, 10))
    dendrogram(linked, orientation='top', distance_sort='descending', show_leaf_counts=True)
    plt.show()


def get_statistics(flows):
    flows_per_pair = {}
    repeated = []
    for flow in flows:
        repeated.append(flow.repeated_count)

    distinct = set(repeated)
    repeated.sort()

    for i in range(len(distinct) + 1):
        flows_per_pair[i] = repeated.count(i)
    x = []
    y = []
    for i in flows_per_pair:
        x.append(i)
        y.append(flows_per_pair[i])

    plt.xlabel("number of flows per pair")
    plt.ylabel("number of pairs")
    plt.title = "comparing number of flows per pairs "
    plt.scatter(x, y)
    plt.show()


def create_json_file(pcap_file_path):
    sniff(offline=pcap_file_path, prn=read_packet_data)
    data = json.dumps(packets_data)
    with open(pcap_file_path.replace('.pcap', '.json'), 'w') as json_file:
        json_file.write(data)


def read_json_file(json_file_path):
    with open(json_file_path, 'r') as json_file:
        cleaned_data = json.load(json_file)
        return cleaned_data


if __name__ == '__main__':
    # create_json_file("./example-01.pcap")
    packets_data = read_json_file("./example-01.json")

    flows = flow_generation(packets_data)

    print("--------All flows count------------")
    print(len(flows))
    print("-----------------------------------")

    filtered_flows = filter_flows(flows)
    print("--------filtered flows count------------")
    print(len(filtered_flows))
    print("-----------------------------------")
    get_statistics(filtered_flows)

    true_dependencies = two_level_dependency_extract(filtered_flows)
    print("--------number of two level dependency flows---------")
    print(len(true_dependencies))
    print("-----------------------------------")

    multi_level_dependencies = multi_level_dependency_extract(true_dependencies)
    print("--------number of multi level dependency flows---------")
    print(len(multi_level_dependencies))
    print("-----------------------------------")

    print("---------distance matrix------------------")
    result = compute_distance(multi_level_dependencies)
    for row in result:
        print(row)

    print("-----------------------------------")

    print("---------plotting------------------")
    cluster_hosts(result)
