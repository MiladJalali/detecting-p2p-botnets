import math

from params import T_DEP, S_DEP_THRESHOLD, FLOW_PACKET_THRESHOLD


class Flow:
    def __init__(self, size=None, host1_ip=None, host2_ip=None, host1_port=None, host2_port=None, start_time=None,
                 end_time=None):
        self.size = size
        self.host1_ip = host1_ip
        self.host2_ip = host2_ip
        self.host1_port = host1_port
        self.host2_port = host2_port
        self.start_time = start_time
        self.end_time = end_time
        self.repeated_count = 0
        self.number_of_packets = 1

    def update(self, new_packet_size, new_packet_send_time):
        self.size += new_packet_size
        self.end_time = new_packet_send_time
        self.number_of_packets += 1

    @property
    def get_duration(self):
        return self.end_time - self.start_time

    def belong_to_this_flow(self, packet):
        if packet['source_ip'] == self.host1_ip and \
                packet['destination_ip'] == self.host2_ip and \
                packet['source_port'] == self.host1_port and \
                packet['destination_port'] == self.host2_port and \
                packet['send_time'] < self.end_time + FLOW_PACKET_THRESHOLD:
            return True
        return packet['source_ip'] == self.host2_ip and \
               packet['destination_ip'] == self.host1_ip and \
               packet['source_port'] == self.host2_port and \
               packet['destination_port'] == self.host1_port and \
               packet['send_time'] < self.end_time + FLOW_PACKET_THRESHOLD

    @classmethod
    def create_new_flow(cls, packet):
        return cls(packet['packet_size'], packet['source_ip'], packet['destination_ip'], packet['source_port'],
                   packet['destination_port'], packet['send_time'], packet['send_time'])

    def is_equal(self, flow):
        return self.size == flow.size and \
               self.get_duration == flow.get_duration and \
               self.host1_ip == flow.host1_ip and \
               self.host2_ip == flow.host2_ip and \
               self.host1_port == flow.host1_port and \
               self.host2_port == flow.host2_port and \
               self.number_of_packets == flow.number_of_packets

    def is_timely_correlated(self, flow):
        return flow.start_time - self.end_time < T_DEP


class FlowDependency:
    def __init__(self, first_flow, last_flow):
        self.first_flow = first_flow
        self.last_flow = last_flow
        self.flows = [first_flow, last_flow]
        self.t_list = [0, ]
        self.scores = list()
        self.multi_level_score = 0

    @staticmethod
    def calculate_score(first_flow_dependency, second_flow_dependency, t):
        return math.sqrt(
            math.pow(t, 2) / (first_flow_dependency.repeated_count * second_flow_dependency.repeated_count))

    def calculate_multi_level_scores(self):
        for index, flow in enumerate(self.flows):
            if index != len(self.flows) - 1:
                self.scores.append(
                    FlowDependency.calculate_score(self.flows[index], self.flows[index + 1], self.t_list[index]))
        self.multi_level_score = math.prod(self.scores)

    def check_dependency(self):
        s_dep = math.sqrt(
            math.pow(self.t_list[0], 2) / (self.first_flow.repeated_count * self.last_flow.repeated_count))
        self.is_true_dependency = s_dep > S_DEP_THRESHOLD
        return s_dep

    def is_equal(self, flow_dependency):
        return self.first_flow.is_equal(flow_dependency.first_flow) and self.last_flow.is_equal(
            flow_dependency.last_flow)

    @property
    def get_dependency_level(self):
        return len(self.t_list) + 1
